from random import randint # Import la fonction qui génère un nombre aléatoire
import time # Importe la fonction qui mesure le temps

matrice = [] # Créer la liste "matrice"

# Fonction afficher le menu du jeu
def afficher_menu():
    print("Menu :")
    print("1. Jouer au jeu")
    print("2. Afficher les 10 derniers résultats")
    print("3. Afficher tous les résultats")

# Fonction gerer le jeu, lancer le chrnonomètre, générer un nombre de 0 à 1000,
# inviter l'utilisateur à entrer des nombres, les comparer ...
def jouer_au_jeu():
    temps_debut = time.time()
    just_price = randint(1, 1000)
    running = True
    user_price = None


    while running:
        user_price = int(input("Entrez un prix : "))

        if user_price == just_price:
            print("Trouvé !")
            running = False
        elif user_price > just_price:
            print("C'est moins")
        elif user_price < just_price:
            print("C'est plus")

    temps_fin = time.time()
    duree = chronometre(temps_debut, temps_fin)
    return user_price, duree

# Fonction écrire dans le fichier .txt
def ecrire_matrice_dans_fichier(Resultats_fichier, matrice):
    with open(Resultats_fichier, 'a') as fichier:
        for ligne in matrice:
            ligne_str = ''.join(map(str, ligne))
            fichier.write(ligne_str + '\n')

    print("Les informations ont été ajoutées au fichier.")

# Fonction afficher les 10 derniers résultats
def afficher_10_derniers_resultats(Resultats_fichier):
    with open(Resultats_fichier, 'r') as fichier:
        lignes = fichier.readlines()
        if len(lignes) >= 10:
            dernieres_lignes = lignes[-10:]
            for ligne in dernieres_lignes:
                print(ligne, end='')
        else:
            for ligne in lignes:
                print(ligne, end='')

# Fonction afficher tout les résultats
def afficher_tous_les_resultats(Resultats_fichier):
    with open(Resultats_fichier, 'r') as fichier:
        lignes = fichier.readlines()
        for ligne in lignes:
            print(ligne, end='')

# Fonction calculer la durée
def chronometre(temps_debut, temps_fin):
    duree = temps_fin - temps_debut
    heures, duree = divmod(duree, 3600)
    minutes, duree = divmod(duree, 60)
    secondes = int(duree)
    print(f"Temps écoulé : {heures} heures, {minutes} minutes, {secondes} secondes")
    return secondes

# Programme principal, afficher menu, choix 1, 2, 3 ou autre
if __name__ == '__main__':
    while True:
        afficher_menu()
        choix = input("Choisissez une option : ")

        if choix == '1':
            user_name = input("Quel est votre nom ?")
            user_price, duree = jouer_au_jeu()
            matrice.append(f" Le joueur {user_name} à trouvé {user_price} en {duree} secondes")
            ecrire_matrice_dans_fichier('Resultats_fichier.txt', matrice)
        elif choix == '2':
            afficher_10_derniers_resultats('Resultats_fichier.txt')
        elif choix == '3':
            afficher_tous_les_resultats('Resultats_fichier.txt')
            break
        else:
            print("Option invalide. Veuillez choisir une option valide.")

